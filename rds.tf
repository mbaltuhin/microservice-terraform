resource "aws_db_subnet_group" "subnetgroup-private-rds" {
    name        = "${var.environment}-rds"
    description = "${var.environment}-rds"
    depends_on = ["aws_subnet.subnet-private1", "aws_subnet.subnet-private2"]
    subnet_ids  = ["${aws_subnet.subnet-private1.id}", "${aws_subnet.subnet-private2.id}"]
}
resource "aws_db_instance" "mssql" {
  depends_on = ["aws_security_group.rds","aws_db_subnet_group.subnetgroup-private-rds"]
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "sqlserver-ex"
  engine_version       = "14.00.1000.169.v1"
  instance_class       = "db.t2.micro"
  identifier           = "${var.environment}"
  username             = "root"
  password             = "rootpassword"
  skip_final_snapshot  = true
  db_subnet_group_name = "${aws_db_subnet_group.subnetgroup-private-rds.name}"
  vpc_security_group_ids = ["${aws_security_group.rds.id}"]
  tags {
    "ApplicationID" = "pass"
    "Schedule" = "bg-office-hours"
  }
}
