resource "aws_ecs_task_definition" "authorization" {
  family = "${var.environment}-${var.authorization_repo_name}-taskdef"

  container_definitions = <<CONTAINER
[
  {
    "name": "${var.environment}-${var.authorization_repo_name}-container",
    "image": "285390311874.dkr.ecr.${var.region}.amazonaws.com/${var.environment}-${var.authorization_repo_name}",
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0
      }
    ],
    "mountPoints": [
      {
        "sourceVolume": "LogFolder",
        "containerPath": "/home/ec2-user/Logs",
        "readOnly": false
      }
    ]
  }
]
CONTAINER

  volume {
    name      = "LogFolder"
    host_path = "/home/ec2-user/Logs"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}

resource "aws_ecs_task_definition" "platform_management" {
  family = "${var.environment}-${var.platform_management_repo_name}-taskdef"

  container_definitions = <<CONTAINER
[
  {
    "name": "${var.environment}-${var.platform_management_repo_name}-container",
    "image": "285390311874.dkr.ecr.${var.region}.amazonaws.com/${var.environment}-${var.platform_management_repo_name}",
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0
      }
    ],
    "mountPoints": [
      {
        "sourceVolume": "LogFolder",
        "containerPath": "/home/ec2-user/Logs",
        "readOnly": false
      }
    ]
  }
]
CONTAINER

  volume {
    name      = "LogFolder"
    host_path = "/home/ec2-user/Logs"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}

resource "aws_ecs_task_definition" "matchmaking" {
  family = "${var.environment}-${var.matchmaking_repo_name}-taskdef"

  container_definitions = <<CONTAINER
[
  {
    "name": "${var.environment}-${var.matchmaking_repo_name}-container",
    "image": "285390311874.dkr.ecr.${var.region}.amazonaws.com/${var.environment}-${var.matchmaking_repo_name}",
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0
      }
    ],
    "mountPoints": [
      {
        "sourceVolume": "LogFolder",
        "containerPath": "/home/ec2-user/Logs",
        "readOnly": false
      }
    ]
  }
]
CONTAINER

  volume {
    name      = "LogFolder"
    host_path = "/home/ec2-user/Logs"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}

resource "aws_ecs_task_definition" "fleet_management" {
  family = "${var.environment}-${var.fleet_management_repo_name}-taskdef"

  container_definitions = <<CONTAINER
[
  {
    "name": "${var.environment}-${var.fleet_management_repo_name}-container",
    "image": "285390311874.dkr.ecr.${var.region}.amazonaws.com/${var.environment}-${var.fleet_management_repo_name}",
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0
      }
    ],
    "mountPoints": [
      {
        "sourceVolume": "LogFolder",
        "containerPath": "/home/ec2-user/Logs",
        "readOnly": false
      }
    ]
  }
]
CONTAINER

  volume {
    name      = "LogFolder"
    host_path = "/home/ec2-user/Logs"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}

resource "aws_ecs_task_definition" "notification" {
  family = "${var.environment}-${var.notification_repo_name}-taskdef"

  container_definitions = <<CONTAINER
[
  {
    "name": "${var.environment}-${var.notification_repo_name}-container",
    "image": "285390311874.dkr.ecr.${var.region}.amazonaws.com/${var.environment}-${var.notification_repo_name}",
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0
      }
    ],
    "mountPoints": [
      {
        "sourceVolume": "LogFolder",
        "containerPath": "/home/ec2-user/Logs",
        "readOnly": false
      }
    ]
  }
]
CONTAINER

  volume {
    name      = "LogFolder"
    host_path = "/home/ec2-user/Logs"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}

resource "aws_ecs_task_definition" "translation" {
  family = "${var.environment}-${var.translation_repo_name}-taskdef"

  container_definitions = <<CONTAINER
[
  {
    "name": "${var.environment}-${var.translation_repo_name}-container",
    "image": "285390311874.dkr.ecr.${var.region}.amazonaws.com/${var.environment}-${var.translation_repo_name}",
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0
      }
    ],
    "mountPoints": [
      {
        "sourceVolume": "LogFolder",
        "containerPath": "/home/ec2-user/Logs",
        "readOnly": false
      }
    ]
  }
]
CONTAINER

  volume {
    name      = "LogFolder"
    host_path = "/home/ec2-user/Logs"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}

resource "aws_ecs_task_definition" "game_data" {
  family = "${var.environment}-${var.game_data_repo_name}-taskdef"

  container_definitions = <<CONTAINER
[
  {
    "name": "${var.environment}-${var.game_data_repo_name}-container",
    "image": "285390311874.dkr.ecr.${var.region}.amazonaws.com/${var.environment}-${var.game_data_repo_name}",
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0
      }
    ],
    "mountPoints": [
      {
        "sourceVolume": "LogFolder",
        "containerPath": "/home/ec2-user/Logs",
        "readOnly": false
      }
    ]
  }
]
CONTAINER

  volume {
    name      = "LogFolder"
    host_path = "/home/ec2-user/Logs"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}

resource "aws_ecs_task_definition" "player_data" {
  family = "${var.environment}-${var.player_data_repo_name}-taskdef"

  container_definitions = <<CONTAINER
[
  {
    "name": "${var.environment}-${var.player_data_repo_name}-container",
    "image": "285390311874.dkr.ecr.${var.region}.amazonaws.com/${var.environment}-${var.player_data_repo_name}",
    "memory": 256,
    "essential": true,
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 0
      }
    ],
    "mountPoints": [
      {
        "sourceVolume": "LogFolder",
        "containerPath": "/home/ec2-user/Logs",
        "readOnly": false
      }
    ]
  }
]
CONTAINER

  volume {
    name      = "LogFolder"
    host_path = "/home/ec2-user/Logs"
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
