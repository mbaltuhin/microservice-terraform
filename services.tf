resource "aws_iam_role" "scale" {
  name = "${var.environment}-ecs-scale-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_ecs_service" "auth" {
  name            = "${var.environment}-${var.authorization_repo_name}"
  depends_on = ["aws_lb_listener_rule.auth", "aws_lb_target_group.auth"]
  cluster         = "${aws_ecs_cluster.services.id}"
  task_definition = "${var.environment}-${var.authorization_repo_name}-taskdef"
  desired_count   = 1
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.auth.arn}"
    container_name = "${var.environment}-${var.authorization_repo_name}-container"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
resource "aws_ecs_service" "platform_management" {
  name            = "${var.environment}-${var.platform_management_repo_name}"
  depends_on = ["aws_lb_listener_rule.platform_management", "aws_lb_target_group.platform_management"]
  cluster         = "${aws_ecs_cluster.services.id}"
  task_definition = "${var.environment}-${var.platform_management_repo_name}-taskdef"
  desired_count   = 1
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.platform_management.arn}"
    container_name = "${var.environment}-${var.platform_management_repo_name}-container"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
resource "aws_ecs_service" "matchmaking" {
  name            = "${var.environment}-${var.matchmaking_repo_name}"
  depends_on = ["aws_lb_listener_rule.matchmaking", "aws_lb_target_group.matchmaking"]
  cluster         = "${aws_ecs_cluster.services.id}"
  task_definition = "${var.environment}-${var.matchmaking_repo_name}-taskdef"
  desired_count   = 1
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.matchmaking.arn}"
    container_name = "${var.environment}-${var.matchmaking_repo_name}-container"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
resource "aws_ecs_service" "fleet_management" {
  name            = "${var.environment}-${var.fleet_management_repo_name}"
  depends_on = ["aws_lb_listener_rule.fleet_management", "aws_lb_target_group.fleet_management"]
  cluster         = "${aws_ecs_cluster.services.id}"
  task_definition = "${var.environment}-${var.fleet_management_repo_name}-taskdef"
  desired_count   = 1
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.fleet_management.arn}"
    container_name = "${var.environment}-${var.fleet_management_repo_name}-container"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
resource "aws_ecs_service" "notification" {
  name            = "${var.environment}-${var.notification_repo_name}"
  depends_on = ["aws_lb_listener_rule.notification", "aws_lb_target_group.notification"]
  cluster         = "${aws_ecs_cluster.services.id}"
  task_definition = "${var.environment}-${var.notification_repo_name}-taskdef"
  desired_count   = 1
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.notification.arn}"
    container_name = "${var.environment}-${var.notification_repo_name}-container"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
resource "aws_ecs_service" "translation" {
  name            = "${var.environment}-${var.translation_repo_name}"
  depends_on = ["aws_lb_listener_rule.translation", "aws_lb_target_group.translation"]
  cluster         = "${aws_ecs_cluster.services.id}"
  task_definition = "${var.environment}-${var.translation_repo_name}-taskdef"
  desired_count   = 1
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.translation.arn}"
    container_name = "${var.environment}-${var.translation_repo_name}-container"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
resource "aws_ecs_service" "game_data" {
  name            = "${var.environment}-${var.game_data_repo_name}"
  depends_on = ["aws_lb_listener_rule.game_data", "aws_lb_target_group.game_data"]
  cluster         = "${aws_ecs_cluster.services.id}"
  task_definition = "${var.environment}-${var.game_data_repo_name}-taskdef"
  desired_count   = 1
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.game_data.arn}"
    container_name = "${var.environment}-${var.game_data_repo_name}-container"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
resource "aws_ecs_service" "player_data" {
  name            = "${var.environment}-${var.player_data_repo_name}"
  depends_on = ["aws_lb_listener_rule.player_data", "aws_lb_target_group.player_data"]
  cluster         = "${aws_ecs_cluster.services.id}"
  task_definition = "${var.environment}-${var.player_data_repo_name}-taskdef"
  desired_count   = 1
  lifecycle {
    ignore_changes = ["desired_count", "task_definition"]
  }

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.player_data.arn}"
    container_name = "${var.environment}-${var.player_data_repo_name}-container"
    container_port = 80
  }

  placement_constraints {
    type       = "memberOf"
    expression = "attribute:ecs.availability-zone in [${data.aws_availability_zones.available.names[0]}, ${data.aws_availability_zones.available.names[1]}]"
  }
}
resource "aws_appautoscaling_target" "ecs_target_authorization" {
  depends_on = ["aws_iam_role.codepipeline_role","aws_ecs_cluster.services","aws_ecs_service.auth"]
  max_capacity       = 1
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.auth.name}"
  role_arn           = "${aws_iam_role.scale.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  lifecycle {
    ignore_changes = ["role_arn"]
  }
}
resource "aws_appautoscaling_target" "ecs_target_game_data" {
  depends_on = ["aws_iam_role.codepipeline_role","aws_ecs_cluster.services","aws_ecs_service.platform_management"]
  max_capacity       = 1
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.game_data.name}"
  role_arn           = "${aws_iam_role.scale.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  lifecycle {
    ignore_changes = ["role_arn"]
  }
}
resource "aws_appautoscaling_target" "ecs_target_player_data" {
  depends_on = ["aws_iam_role.codepipeline_role","aws_ecs_cluster.services","aws_ecs_service.player_data"]
  max_capacity       = 1
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.player_data.name}"
  role_arn           = "${aws_iam_role.scale.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  lifecycle {
    ignore_changes = ["role_arn"]
  }
}
resource "aws_appautoscaling_target" "ecs_target_platform_management" {
  depends_on = ["aws_iam_role.codepipeline_role","aws_ecs_cluster.services","aws_ecs_service.platform_management"]
  max_capacity       = 1
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.platform_management.name}"
  role_arn           = "${aws_iam_role.scale.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  lifecycle {
    ignore_changes = ["role_arn"]
  }
}
resource "aws_appautoscaling_target" "ecs_target_fleet_management" {
  depends_on = ["aws_iam_role.codepipeline_role","aws_ecs_cluster.services","aws_ecs_service.fleet_management"]
  max_capacity       = 1
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.fleet_management.name}"
  role_arn           = "${aws_iam_role.scale.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  lifecycle {
    ignore_changes = ["role_arn"]
  }
}
resource "aws_appautoscaling_target" "ecs_target_translation" {
  depends_on = ["aws_iam_role.codepipeline_role","aws_ecs_cluster.services","aws_ecs_service.translation"]
  max_capacity       = 1
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.translation.name}"
  role_arn           = "${aws_iam_role.scale.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  lifecycle {
    ignore_changes = ["role_arn"]
  }
}
resource "aws_appautoscaling_target" "ecs_target_notification" {
  depends_on = ["aws_iam_role.codepipeline_role","aws_ecs_cluster.services","aws_ecs_service.notification"]
  max_capacity       = 1
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.notification.name}"
  role_arn           = "${aws_iam_role.scale.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  lifecycle {
    ignore_changes = ["role_arn"]
  }
}
resource "aws_appautoscaling_target" "ecs_target_matchmaking" {
  depends_on = ["aws_iam_role.codepipeline_role","aws_ecs_cluster.services","aws_ecs_service.matchmaking"]
  max_capacity       = 1
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.matchmaking.name}"
  role_arn           = "${aws_iam_role.scale.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
  lifecycle {
    ignore_changes = ["role_arn"]
  }
}
resource "aws_appautoscaling_policy" "auth_scale_up" {
  depends_on = ["aws_appautoscaling_target.ecs_target_authorization"]
  name                    = "scale-up"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.auth.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}
resource "aws_appautoscaling_policy" "auth_scale_down" {
  depends_on = ["aws_appautoscaling_target.ecs_target_authorization"]
  name                    = "scale-down"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.auth.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
resource "aws_appautoscaling_policy" "platform_management_scale_scale_up" {
  depends_on = ["aws_appautoscaling_target.ecs_target_platform_management"]
  name                    = "scale-up"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.platform_management.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}
resource "aws_appautoscaling_policy" "platform_management_scale_down" {
  depends_on = ["aws_appautoscaling_target.ecs_target_platform_management"]
  name                    = "scale-down"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.platform_management.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
resource "aws_appautoscaling_policy" "fleet_management_scale_up" {
  depends_on = ["aws_appautoscaling_target.ecs_target_fleet_management"]
  name                    = "scale-up"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.fleet_management.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}
resource "aws_appautoscaling_policy" "fleet_management_scale_down" {
  depends_on = ["aws_appautoscaling_target.ecs_target_fleet_management"]
  name                    = "scale-down"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.fleet_management.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
resource "aws_appautoscaling_policy" "translation_scale_up" {
  depends_on = ["aws_appautoscaling_target.ecs_target_translation"]
  name                    = "scale-up"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.translation.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}
resource "aws_appautoscaling_policy" "translation_scale_down" {
  depends_on = ["aws_appautoscaling_target.ecs_target_translation"]
  name                    = "scale-down"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.translation.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
resource "aws_appautoscaling_policy" "notification_scale_up" {
  depends_on = ["aws_appautoscaling_target.ecs_target_notification"]
  name                    = "scale-up"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.notification.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}
resource "aws_appautoscaling_policy" "notification_scale_down" {
  depends_on = ["aws_appautoscaling_target.ecs_target_notification"]
  name                    = "scale-down"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.notification.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
resource "aws_appautoscaling_policy" "matchmaking_scale_up" {
  depends_on = ["aws_appautoscaling_target.ecs_target_matchmaking"]
  name                    = "scale-up"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.matchmaking.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}
resource "aws_appautoscaling_policy" "matchmaking_scale_down" {
  depends_on = ["aws_appautoscaling_target.ecs_target_matchmaking"]
  name                    = "scale-down"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.matchmaking.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
resource "aws_appautoscaling_policy" "player_data_scale_up" {
  depends_on = ["aws_appautoscaling_target.ecs_target_player_data"]
  name                    = "scale-up"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.player_data.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}
resource "aws_appautoscaling_policy" "player_data_scale_down" {
  depends_on = ["aws_appautoscaling_target.ecs_target_player_data"]
  name                    = "scale-down"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.player_data.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
resource "aws_appautoscaling_policy" "game_data_scale_up" {
  depends_on = ["aws_appautoscaling_target.ecs_target_game_data"]
  name                    = "scale-up"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.game_data.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }
}
resource "aws_appautoscaling_policy" "game_data_scale_down" {
  depends_on = ["aws_appautoscaling_target.ecs_target_game_data"]
  name                    = "scale-down"
  resource_id             = "service/${aws_ecs_cluster.services.name}/${aws_ecs_service.game_data.name}"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      scaling_adjustment          = -1
    }
  }
}
