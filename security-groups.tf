resource "aws_security_group" "redis" {
  name        = "${var.environment}-redis"
  description = "Security group for Redis in ${var.environment} environment"
  depends_on = ["aws_vpc.alpha-environment"]
  vpc_id      = "${aws_vpc.alpha-environment.id}"

  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    security_groups = ["${aws_security_group.services.id}"]
    description = "Allows all TCP traffic on port 6379 for Redis"
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = ["${aws_security_group.vpn_instance.id}"]
    description = "Allows all traffic on all ports from VPN instance(s)"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows outgoing traffic"
  }

  tags {
    "Name" = "${var.environment}-redis"
    "ApplicationID" = "pass"
  }
}
resource "aws_security_group" "rds" {
  depends_on = ["aws_vpc.alpha-environment", "aws_security_group.services"]
  name        = "${var.environment}-rds-mssql"
  description = "${var.environment}-rds-mssql"
  vpc_id      = "${aws_vpc.alpha-environment.id}"


  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    security_groups = ["${aws_security_group.services.id}"]
    description = "Allows all traffic"
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = ["${aws_security_group.vpn_instance.id}"]
    description = "Allows all traffic on all ports from VPN instance(s)"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all traffic"
  }

  tags {
    "Name" = "${var.environment}-rds-mssql"
    "ApplicationID" = "pass"
  }
}
resource "aws_security_group" "es" {
  depends_on = ["aws_vpc.alpha-environment", "aws_security_group.services"]
  name        = "${var.environment}-elasticsearch"
  description = "${var.environment}-elasticsearch"
  vpc_id      = "${aws_vpc.alpha-environment.id}"


  tags {
    "Name" = "${var.environment}-elasticsearch"
    "ApplicationID" = "pass"
  }
}
resource "aws_security_group" "load_balancer" {
  depends_on = ["aws_vpc.alpha-environment"]
  name        = "${var.environment}-services-alb"
  description = "ALB security group"
  vpc_id      = "${aws_vpc.alpha-environment.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all TCP traffic on port 80"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all TCP traffic on port 443 (SSL)"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all outgoing trafic"
  }

  tags {
    "Name" = "${var.environment}-services-alb"
    "ApplicationID" = "pass"
  }
}
resource "aws_security_group" "game_load_balancer" {
  depends_on = ["aws_vpc.alpha-environment"]
  name        = "${var.environment}-game-alb"
  description = "ALB security group"
  vpc_id      = "${aws_vpc.alpha-environment.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all TCP traffic on port 80"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all TCP traffic on port 443 (SSL)"
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = ["${aws_security_group.vpn_instance.id}"]
    description = "Allows all traffic on all ports from VPN instance(s)"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all outgoing trafic"
  }

  tags {
    "Name" = "${var.environment}-game-alb"
    "ApplicationID" = "pass"
  }
}
resource "aws_security_group" "game-servers" {
  depends_on = ["aws_vpc.alpha-environment"]
  name        = "${var.environment}-game-hosts"
  description = "${var.environment}-game-hosts"
  vpc_id      = "${aws_vpc.alpha-environment.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = ["${aws_security_group.vpn_instance.id}"]
    description = "Allows all traffic on all ports from VPN instance(s)"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all traffic"
  }

  tags {
    "Name" = "${var.environment}-game-hosts"
    "ApplicationID" = "pass"
  }
}
resource "aws_security_group" "services" {
  depends_on = ["aws_vpc.alpha-environment"]
  name        = "${var.environment}-services"
  description = "${var.environment}-services"
  vpc_id      = "${aws_vpc.alpha-environment.id}"

  tags {
    "Name" = "${var.environment}-services"
    "ApplicationID" = "pass"
  }
}
resource "aws_security_group_rule" "services1" {
  type        = "ingress"
  description = "Allows all TCP traffic from the Load Balancer"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  source_security_group_id = "${aws_security_group.load_balancer.id}"

  security_group_id = "${aws_security_group.services.id}"
}

resource "aws_security_group_rule" "services2" {
  type        = "ingress"
  description = "Allows all traffic on all ports from VPN instance(s)"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  source_security_group_id = "${aws_security_group.vpn_instance.id}"
  security_group_id = "${aws_security_group.services.id}"
}

resource "aws_security_group_rule" "services3" {
  type        = "ingress"
  description = "Allows TCP traffic on port 2049 from EFS volumes"
  from_port   = "2049"
  to_port     = "2049"
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.services.id}"
}
resource "aws_security_group_rule" "services4" {
  type        = "egress"
  description = "Allows all traffic"
  from_port   = "0"
  to_port     = "0"
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.services.id}"
}


resource "aws_security_group_rule" "es1" {
  depends_on = ["aws_security_group.es"]
  type        = "ingress"
  description = "Allows all traffic on all ports from VPN instance(s)"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  source_security_group_id = "${aws_security_group.vpn_instance.id}"
  security_group_id = "${aws_security_group.es.id}"
}
resource "aws_security_group_rule" "es2" {
  depends_on = ["aws_security_group.es"]
  type        = "ingress"
  description = "Allows all traffic on all ports from service instance(s)"
  from_port   = "0"
  to_port     = "0"
  protocol    = "-1"
  source_security_group_id = "${aws_security_group.services.id}"
  security_group_id = "${aws_security_group.es.id}"
}

resource "aws_security_group_rule" "es3" {
  type        = "egress"
  description = "Allows all traffic"
  from_port   = "0"
  to_port     = "0"
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.es.id}"
}
resource "aws_security_group" "vpn_instance" {
  depends_on = ["aws_vpc.alpha-environment"]
  name        = "${var.environment}-openvpn"
  description = "${var.environment}-openvpn"
  vpc_id      = "${aws_vpc.alpha-environment.id}"

  ingress {
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all UDP traffic on port 1194 (VPN)"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all SSH traffic"
  }

  ingress {
    from_port   = 943
    to_port     = 943
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all TCP traffic on port 943 (Admin Panel)"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows SSL traffic on port 443"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all outgoing traffic"
  }

  tags {
    "Name" = "${var.environment}-openvpn"
    "ApplicationID" = "pass"
  }
}
resource "aws_security_group" "efs_volume" {
  depends_on = ["aws_vpc.alpha-environment"]
  name        = "${var.environment}-efs"
  description = "${var.environment}-efs"
  vpc_id      = "${aws_vpc.alpha-environment.id}"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    security_groups = ["${aws_security_group.services.id}"]
    description = "Allows all traffic on all ports from VPN instance(s)"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allows all outgoing traffic"
  }

  tags {
    "Name" = "${var.environment}-openvpn"
    "ApplicationID" = "pass"
  }
}
