resource "aws_ecs_cluster" "game" {
  name = "${var.environment}-game"
}
resource "aws_ecs_cluster" "services" {
  name = "${var.environment}-services"
}
