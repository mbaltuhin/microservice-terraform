resource "aws_iam_role" "deploy" {
  name = "${var.environment}-deploy-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "example" {
  name = "${var.environment}-deploy-policy"
  role = "${aws_iam_role.deploy.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:CompleteLifecycleAction",
        "autoscaling:DeleteLifecycleHook",
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeLifecycleHooks",
        "autoscaling:PutLifecycleHook",
        "autoscaling:RecordLifecycleActionHeartbeat",
        "codedeploy:*",
        "ec2:DescribeInstances",
        "ec2:DescribeInstanceStatus",
        "tag:GetTags",
        "tag:GetResources",
        "sns:Publish"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}
resource "aws_s3_bucket" "foo" {
  acl    = "private"
  force_destroy = true
}

resource "aws_iam_role" "codepipeline_role" {
  name = "${var.environment}-pipeline-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codepipeline.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "codepipeline_policy" {
  depends_on = ["aws_iam_role.codepipeline_role"]
  name = "${var.environment}-codepipeline_policy"
  role = "${aws_iam_role.codepipeline_role.id}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect":"Allow",
      "Action": "*",
      "Resource": "*"
    }
  ]
}
EOF
}
resource "aws_kms_key" "a" {}

resource "aws_kms_alias" "a" {
  name          = "alias/${var.environment}"
  target_key_id = "${aws_kms_key.a.key_id}"
}



resource "aws_codepipeline" "authorization" {
  depends_on = ["aws_codebuild_project.authorization","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo"]
  name     = "${var.authorization_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        S3Bucket = "${var.repo_bucket}"
        S3ObjectKey = "${var.authorization_repo_name}.zip"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.authorization.name}"
      }
    }
  }
}
resource "aws_codepipeline" "matchmaking" {
  depends_on = ["aws_codebuild_project.matchmaking","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo"]
  name     = "${var.matchmaking_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        S3Bucket = "${var.repo_bucket}"
        S3ObjectKey = "${var.matchmaking_repo_name}.zip"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.matchmaking.name}"
      }
    }
  }
}
resource "aws_codepipeline" "platform_management" {
  depends_on = ["aws_codebuild_project.platform_management","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo"]
  name     = "${var.platform_management_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        S3Bucket = "${var.repo_bucket}"
        S3ObjectKey = "${var.platform_management_repo_name}.zip"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.platform_management.name}"
      }
    }
  }
}
resource "aws_codepipeline" "fleet_management" {
  depends_on = ["aws_codebuild_project.fleet_management","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo"]
  name     = "${var.fleet_management_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        S3Bucket = "${var.repo_bucket}"
        S3ObjectKey = "${var.fleet_management_repo_name}.zip"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.fleet_management.name}"
      }
    }
  }
}
resource "aws_codepipeline" "notification" {
  depends_on = ["aws_codebuild_project.notification","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo"]
  name     = "${var.notification_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        S3Bucket = "${var.repo_bucket}"
        S3ObjectKey = "${var.notification_repo_name}.zip"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.notification.name}"
      }
    }
  }
}
resource "aws_codepipeline" "translation" {
  depends_on = ["aws_codebuild_project.admin","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo"]
  name     = "${var.translation_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        S3Bucket = "${var.repo_bucket}"
        S3ObjectKey = "${var.translation_repo_name}.zip"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.translation.name}"
      }
    }
  }
}
resource "aws_codepipeline" "game_data" {
  depends_on = ["aws_codebuild_project.game_data","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo"]
  name     = "${var.game_data_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        S3Bucket = "${var.repo_bucket}"
        S3ObjectKey = "${var.game_data_repo_name}.zip"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.game_data.name}"
      }
    }
  }
}
resource "aws_codepipeline" "player_data" {
  depends_on = ["aws_codebuild_project.player_data","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo"]
  name     = "${var.player_data_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "S3"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        S3Bucket = "${var.repo_bucket}"
        S3ObjectKey = "${var.player_data_repo_name}.zip"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.player_data.name}"
      }
    }
  }
}
resource "aws_codepipeline" "admin" {
  depends_on = ["aws_codebuild_project.admin","aws_iam_role_policy.codepipeline_policy", "aws_s3_bucket.foo", "aws_autoscaling_group.alpha-admin-tool"]
  name     = "${var.admin_repo_name}"
  role_arn = "${aws_iam_role.codepipeline_role.arn}"

  artifact_store {
    location = "${aws_s3_bucket.foo.bucket}"
    type     = "S3"
    encryption_key {
      id   = "${aws_kms_key.a.arn}"
      type = "KMS"
    }
  }

  stage {
    name = "Source"

    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version         = "1"
      output_artifacts = ["src"]

      configuration {
        RepositoryName = "${var.admin_repo_name}"
        BranchName = "master"
      }
    }
  }

  stage {
    name = "Build"

    action {
      name            = "Build"
      category        = "Build"
      owner           = "AWS"
      provider        = "CodeBuild"
      input_artifacts = ["src"]
      output_artifacts = ["app"]
      version         = "1"

      configuration {
        ProjectName = "${aws_codebuild_project.admin.name}"
      }
    }
  }
  stage {
    name = "Deploy"
    action  {
      name = "Deploy"
      category = "Deploy"
      owner = "AWS"
      provider = "CodeDeploy"
      input_artifacts = ["app"]
      version = "1"
      configuration {
        ApplicationName = "${aws_codedeploy_app.app.name}"
        DeploymentGroupName = "${aws_codedeploy_deployment_group.admin.deployment_group_name}"
      }
    }
  }
}
resource "aws_codedeploy_deployment_config" "foo" {
  deployment_config_name = "${var.environment}-${var.admin_repo_name}"

  minimum_healthy_hosts {
    type  = "HOST_COUNT"
    value = 1
  }
}
resource "aws_codedeploy_app" "app" {
  name = "${var.admin_repo_name}"
}
resource "aws_codedeploy_deployment_group" "admin" {
  app_name               = "${aws_codedeploy_app.app.name}"
  deployment_group_name  = "${var.admin_repo_name}"
  service_role_arn       = "${aws_iam_role.deploy.arn}"
  deployment_config_name = "CodeDeployDefault.AllAtOnce"

  ec2_tag_filter {
    key   = "aws:autoscaling:groupName"
    type  = "KEY_AND_VALUE"
    value = "${var.environment}-${var.admin_repo_name}"
  }


  auto_rollback_configuration {
    enabled = false
    events  = ["DEPLOYMENT_FAILURE"]
  }
}
