data "aws_ami" "amazon-linux" {
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
  filter {
    name   = "name"
    values = ["*amzn-ami-2017.09.j-amazon-ecs-optimized*"]
  }

  most_recent = true
}
data "aws_iam_policy_document" "ecsinstancerole_assume_role_policy" {
  statement {
    actions = [ "sts:AssumeRole" ]
    principals {
      type = "Service"
      identifiers = [ "ec2.amazonaws.com" ]
    }
  }
}
data "aws_ami" "ubuntu" {
    most_recent = true

    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
    }

    filter {
        name   = "virtualization-type"
        values = ["hvm"]
    }

    owners = ["099720109477"] # Canonical
}

data "aws_iam_policy_document" "ecs_instance_role_document" {
  statement {
    actions = [
      "s3:*"
    ]
    resources = ["*"]
  }

  statement {
    actions = [
      "ecs:*",
      "ecr:*",
      "ec2:*",
      "es:*",
      "dynamodb:*",
      "sqs:*",
      "rds:*",
      "elasticache:*",
      "logs:CreateLogStream",
      "cloudwatch:PutMetricData",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }
  statement {
      effect = "Allow"
      actions = [
         "sqs:*"
      ],
      resources = ["arn:aws:sqs:${data.aws_region.current.name}:*"]
   }

}

resource "aws_iam_role" "core_instance_role" {
  name = "${var.environment}-ecs-instance-role"
  assume_role_policy = "${data.aws_iam_policy_document.ecsinstancerole_assume_role_policy.json}"
}

resource "aws_iam_role_policy" "core_instance" {
  name = "${var.environment}-ecs-instance-role"
  depends_on = ["data.aws_iam_policy_document.ecs_instance_role_document"]
  role = "${aws_iam_role.core_instance_role.id}"
  policy = "${data.aws_iam_policy_document.ecs_instance_role_document.json}"
}

resource "aws_iam_instance_profile" "core_instance" {
	name = "${var.environment}-ecs-instance-role"
	role = "${aws_iam_role.core_instance_role.id}"
}


resource "aws_launch_configuration" "alpha-admin-tool" {
    depends_on = ["aws_security_group.services","aws_subnet.subnet-public1"]
    name                        = "${var.environment}-admin-tool"
    image_id                    = "${data.aws_ami.ubuntu.id}"
    instance_type               = "${var.ec2_type}"
    security_groups = ["${aws_security_group.services.id}"]
    key_name = "${var.key_pair_name}"
    iam_instance_profile = "${aws_iam_instance_profile.core_instance.id}"
    user_data = <<EOF
#!/bin/bash
apt-get update && apt-get install apache2 wget ruby -y
ufw allow 'Apache Full'
cd /home/ubuntu
wget "https://aws-codedeploy-${var.region}.s3.amazonaws.com/latest/install"
chmod +x ./install
./install auto
service codedeploy-agent start
systemctl start apache2
EOF
}

resource "aws_launch_configuration" "alpha-service-hosts" {
    depends_on = ["aws_security_group.services","aws_subnet.subnet-private2"]
    name                        = "${var.environment}-service-hosts"
    image_id                    = "${data.aws_ami.amazon-linux.id}"
    instance_type               = "${var.ec2_type}"
    security_groups = ["${aws_security_group.services.id}"]
    key_name = "${var.key_pair_name}"
    iam_instance_profile = "${aws_iam_instance_profile.core_instance.id}"
    ebs_block_device {
      device_name = "/dev/sdc"
      volume_type = "gp2"
      volume_size = "20"
    }
    user_data = <<EOF
#!/bin/bash
echo "ECS_CLUSTER=${aws_ecs_cluster.services.name}" >> /etc/ecs/ecs.config
mkdir -p /home/ec2-user/Logs /EFS /EBS/core-dumps
chmod 777 /EBS -R && chown ec2-user /EBS -R
chmod 777 /EFS -R && chown ec2-user /EFS -R
chmod 777 /home/ec2-user/Logs -R && chown ec2-user /home/ec2-user/Logs -R
if [[ $(file -s /dev/nvme1n1) =~ "huge files" ]] ; then yes | mkfs -t ext4 /dev/nvme1n1 && mount /dev/nvme1n1 /EBS ; else yes | mkfs -t ext4 /dev/nvme2n1 && mount /dev/nvme2n1 /EBS ; fi
echo "/EBS/core-dumps/core_%e.%t" > /proc/sys/kernel/core_pattern
yum install nano -y
touch /etc/yum.repos.d/logstash.repo
cat <<EOT > /etc/yum.repos.d/logstash.repo
[logstash-6.x]
name=Elastic repository for 6.x packages
baseurl=https://artifacts.elastic.co/packages/6.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
EOT
sudo yum clean all
yum update -y ecs-init
service docker restart
start ecs
yum install perl-Switch perl-DateTime cronie perl-Sys-Syslog perl-LWP-Protocol-https curl perl-XML-LibXML perl-digest-SHA perl-Digest-SHA zip unzip wget nano nfs-utils -y
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u172-b11/a58eab1ec242421181065cdc37240b08/jdk-8u172-linux-x64.rpm"
yum localinstall jdk-8u172-linux-x64.rpm -y
sh -c "echo export JAVA_HOME=/usr/java/jdk1.8.0_172/jre >> /etc/environment"
source /etc/environment
service crond start
mkdir ~/CloudWatchMonitoringScripts && cd ~/CloudWatchMonitoringScripts
curl http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -O
unzip CloudWatchMonitoringScripts-1.2.1.zip
rm CloudWatchMonitoringScripts-1.2.1.zip
cd ~/CloudWatchMonitoringScripts/aws-scripts-mon
touch awscreds.conf && chmod 777 awscreds.conf
curl 169.254.169.254/latest/meta-data/instance-id >> /home/ec2-user/.instanceid
source /etc/ecs/ecs.config && echo $ECS_CLUSTER >> /home/ec2-user/.cluster
curl -s 169.254.169.254/latest/dynamic/instance-identity/document | python -c "import json,sys; print json.loads(sys.stdin.read())['region']" >> /home/ec2-user/.region
echo "export CLUSTER='$(cat /home/ec2-user/.cluster)'" >> ~/.bashrc
echo "export REGION='$(cat /home/ec2-user/.region)'" >> ~/.bashrc
echo "export INSTANCEID='$(cat /home/ec2-user/.instanceid)'" >> ~/.bashrc
echo "CLUSTER='$(cat /home/ec2-user/.cluster)'" >> /etc/default/logstash
echo "REGION='$(cat /home/ec2-user/.region)'" >> /etc/default/logstash
echo "INSTANCEID='$(cat /home/ec2-user/.instanceid)'" >> /etc/default/logstash
source ~/.bashrc
echo "* * * * * ~/CloudWatchMonitoringScripts/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --memory-units=gigabytes" >> mycron
echo "* * * * * docker rm -v $(docker ps -aq -f 'status=exited')" >> mycron
echo "* * * * * docker rmi $(docker images -aq -f 'dangling=true')" >> mycron
echo "0 0 * * * rm -rf /var/log/logstash/logstash-plain-*" >> mycron
crontab mycron && rm mycron -f
rpm --import "https://artifacts.elastic.co/GPG-KEY-elasticsearch" && yum install logstash -y
touch /etc/logstash/conf.d/logstash.conf
mkdir -p /usr/share/logstash/config
cp /etc/logstash/pipelines.yml /usr/share/logstash/config
cat <<EOL > /etc/logstash/conf.d/logstash.conf
input {
    file {
        path => "/home/ec2-user/Logs/**/*.log"
        stat_interval => 30
      }
}
filter {
  mutate {
    add_field => {
      "Cluster" => "$${CLUSTER}"
      "InstanceID" => "$${INSTANCEID}"
      "Region" => "$${REGION}"
    }
  }
}
output{
    elasticsearch {
        hosts => ["https://${aws_elasticsearch_domain.es.endpoint}:443"]
    }
}
EOL
chmod 777 /var/log/logstash -R
chmod 777 /var/lib -R
chmod 777 /usr/share/logstash -R
service crond restart
echo 'pipeline.workers: 4' >> /etc/logstash/logstash.yml
echo 'pipeline.batch.size: 250' >> /etc/logstash/logstash.yml
sed -i 's/-Xms256m/-Xms1g/g' >> /etc/logstash/jvm.options
sed -i 's/-Xmx1g/-Xmx2g/g' /etc/logstash/jvm.options
mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${aws_efs_file_system.efs.id}.efs.${var.region}.amazonaws.com:/ /EFS
/usr/share/logstash/bin/logstash -l /var/log/logstash/log.log &
curl -X PUT -H "Content-Type: application/json" -d '{"index":{"number_of_replicas" : 0}}' 'https://${aws_elasticsearch_domain.es.endpoint}/logstash-*/_settings'
EOF
}
resource "aws_launch_configuration" "alpha-game-hosts" {
    depends_on = ["aws_security_group.game-servers","aws_subnet.subnet-public1"]
    name                        = "${var.environment}-game-host"
    image_id                    = "${data.aws_ami.amazon-linux.id}"
    instance_type               = "${var.ec2_type}"
    security_groups = ["${aws_security_group.game-servers.id}"]
    key_name = "${var.key_pair_name}"
    iam_instance_profile = "${aws_iam_instance_profile.core_instance.id}"
    ebs_block_device {
      device_name = "/dev/sdh"
      volume_type = "gp2"
      volume_size = "20"
    }
    user_data = <<EOF
#!/bin/bash
echo "ECS_CLUSTER=${aws_ecs_cluster.services.name}" >> /etc/ecs/ecs.config
mkdir -p /home/ec2-user/Logs /EFS /EBS/core-dumps
chmod 777 /EBS -R && chown ec2-user /EBS -R
chmod 777 /EFS -R && chown ec2-user /EFS -R
chmod 777 /home/ec2-user/Logs -R && chown ec2-user /home/ec2-user/Logs -R
if [[ $(file -s /dev/nvme1n1) =~ "huge files" ]] ; then yes | mkfs -t ext4 /dev/nvme1n1 && mount /dev/nvme1n1 /EBS ; else yes | mkfs -t ext4 /dev/nvme2n1 && mount /dev/nvme2n1 /EBS ; fi
echo "/EBS/core-dumps/core_%e.%t" > /proc/sys/kernel/core_pattern
yum install nano -y
touch /etc/yum.repos.d/logstash.repo
cat <<EOT > /etc/yum.repos.d/logstash.repo
[logstash-6.x]
name=Elastic repository for 6.x packages
baseurl=https://artifacts.elastic.co/packages/6.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
EOT
sudo yum clean all
yum update -y ecs-init
service docker restart
start ecs
yum install perl-Switch perl-DateTime cronie perl-Sys-Syslog perl-LWP-Protocol-https curl perl-XML-LibXML perl-digest-SHA perl-Digest-SHA zip unzip wget nano nfs-utils -y
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u172-b11/a58eab1ec242421181065cdc37240b08/jdk-8u172-linux-x64.rpm"
yum localinstall jdk-8u172-linux-x64.rpm -y
sh -c "echo export JAVA_HOME=/usr/java/jdk1.8.0_172/jre >> /etc/environment"
source /etc/environment
service crond start
mkdir ~/CloudWatchMonitoringScripts && cd ~/CloudWatchMonitoringScripts
curl http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -O
unzip CloudWatchMonitoringScripts-1.2.1.zip
rm CloudWatchMonitoringScripts-1.2.1.zip
cd ~/CloudWatchMonitoringScripts/aws-scripts-mon
touch awscreds.conf && chmod 777 awscreds.conf
curl 169.254.169.254/latest/meta-data/instance-id >> /home/ec2-user/.instanceid
source /etc/ecs/ecs.config && echo $ECS_CLUSTER >> /home/ec2-user/.cluster
curl -s 169.254.169.254/latest/dynamic/instance-identity/document | python -c "import json,sys; print json.loads(sys.stdin.read())['region']" >> /home/ec2-user/.region
echo "export CLUSTER='$(cat /home/ec2-user/.cluster)'" >> ~/.bashrc
echo "export REGION='$(cat /home/ec2-user/.region)'" >> ~/.bashrc
echo "export INSTANCEID='$(cat /home/ec2-user/.instanceid)'" >> ~/.bashrc
echo "CLUSTER='$(cat /home/ec2-user/.cluster)'" >> /etc/default/logstash
echo "REGION='$(cat /home/ec2-user/.region)'" >> /etc/default/logstash
echo "INSTANCEID='$(cat /home/ec2-user/.instanceid)'" >> /etc/default/logstash
source ~/.bashrc
echo "* * * * * ~/CloudWatchMonitoringScripts/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --memory-units=gigabytes" >> mycron
echo "* * * * * docker rm -v $(docker ps -aq -f 'status=exited')" >> mycron
echo "* * * * * docker rmi $(docker images -aq -f 'dangling=true')" >> mycron
echo "0 0 * * * rm -rf /var/log/logstash/logstash-plain-*" >> mycron
crontab mycron && rm mycron -f
rpm --import "https://artifacts.elastic.co/GPG-KEY-elasticsearch" && yum install logstash -y
touch /etc/logstash/conf.d/logstash.conf
mkdir -p /usr/share/logstash/config
cp /etc/logstash/pipelines.yml /usr/share/logstash/config
cat <<EOL > /etc/logstash/conf.d/logstash.conf
input {
    file {
        path => "/home/ec2-user/Logs/**/*.log"
        stat_interval => 30
      }
}
filter {
  mutate {
    add_field => {
      "Cluster" => "$${CLUSTER}"
      "InstanceID" => "$${INSTANCEID}"
      "Region" => "$${REGION}"
    }
  }
}
output{
    elasticsearch {
        hosts => ["https://${aws_elasticsearch_domain.es.endpoint}:443"]
    }
}
EOL
chmod 777 /var/log/logstash -R
chmod 777 /var/lib -R
chmod 777 /usr/share/logstash -R
service crond restart
echo 'pipeline.workers: 4' >> /etc/logstash/logstash.yml
echo 'pipeline.batch.size: 250' >> /etc/logstash/logstash.yml
sed -i 's/-Xms256m/-Xms1g/g' >> /etc/logstash/jvm.options
sed -i 's/-Xmx1g/-Xmx2g/g' /etc/logstash/jvm.options
mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 ${aws_efs_file_system.efs.id}.efs.${var.region}.amazonaws.com:/ /EFS
/usr/share/logstash/bin/logstash -l /var/log/logstash/log.log &
curl -X PUT -H "Content-Type: application/json" -d '{"index":{"number_of_replicas" : 0}}' 'https://${aws_elasticsearch_domain.es.endpoint}/logstash-*/_settings'
EOF
}
