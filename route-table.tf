resource "aws_route_table" "public" {
    depends_on = ["aws_internet_gateway.alpha-gateway"]
    vpc_id = "${aws_vpc.alpha-environment.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.alpha-gateway.id}"
    }

    tags {
        "Name" = "${var.environment}-public"
        "ApplicationID" = "pass"
    }
}
resource "aws_route_table" "private" {
    depends_on = ["aws_internet_gateway.alpha-gateway"]
    vpc_id = "${aws_vpc.alpha-environment.id}"
    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${aws_nat_gateway.nat.id}"
    }

    tags {
        "Name" = "${var.environment}-private"
        "ApplicationID" = "pass"
    }
}
resource "aws_route_table_association" "private" {
    depends_on = ["aws_route_table.public"]
    route_table_id = "${aws_route_table.private.id}"
    subnet_id = "${aws_subnet.subnet-private1.id}"
}

resource "aws_route_table_association" "private2" {
    depends_on = ["aws_route_table.public"]
    route_table_id = "${aws_route_table.private.id}"
    subnet_id = "${aws_subnet.subnet-private2.id}"
}

resource "aws_route_table_association" "public" {
    depends_on = ["aws_route_table.public"]
    route_table_id = "${aws_route_table.public.id}"
    subnet_id = "${aws_subnet.subnet-public1.id}"
}

resource "aws_route_table_association" "public2" {
    depends_on = ["aws_route_table.public"]
    route_table_id = "${aws_route_table.public.id}"
    subnet_id = "${aws_subnet.subnet-public2.id}"
}
