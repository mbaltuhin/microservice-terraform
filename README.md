# README #

This README will help you install and use this terraform script

### What is this repository for? ###

* Installation and usage description
* Version 1.0.0

### PREREQUIZITES ###

* Download terraform executable for your OS from https://www.terraform.io/downloads.html
* Obtain your secret and access key from AWS console and ensure your user has Administrative access
* Create a key pair from the EC2 tab in AWS console and download it to the computer you use
* Ensure S3 had a bucket with name rnd-bitbucket-repos containing zips with service source (created from BitBucket pipelines)

### How to use ###

* Open a command prompt and change directory to this repository
* Open 'variables.tf' and setup what environment you will create
* Open 'terraform.tfvars' and fill in your secret key and access key
* Run 'terraform plan' so you ensure that you have a connection to AWS
* To change only specific resources run 'terraform apply -target=resource.name'
* The '-target' flag can be passed multiple times

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact