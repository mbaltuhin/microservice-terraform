resource "aws_eip" "vpn_elastic_ip" {
    vpc      = true
    instance    = "${aws_instance.vpn.id}"
}
data "aws_ami" "vpn" {
  owners = ["679593333241"]
  filter {
    name   = "name"
    values = ["*2.5.0-fe8020db-5343-4c43-9e65-5ed4a825c931-ami-548e4429.4*"]
  }

  most_recent = true
}
resource "aws_instance" "vpn" {
  ami           = "${data.aws_ami.vpn.id}"
  instance_type = "t2.micro"
  key_name      = "${var.key_pair_name}"
  vpc_security_group_ids = ["${aws_security_group.vpn_instance.id}"]
  subnet_id     = "${aws_subnet.subnet-public1.id}"

  tags {
    Name = "OpenVPN Access Server"
    "ApplicationID" = "pass"
    "Schedule" = "bg-office-hours"
  }
}
