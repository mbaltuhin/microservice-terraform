resource "aws_lb" "services-alb" {
  name            = "${var.environment}-service-alb"
  depends_on = ["aws_autoscaling_group.alpha-service-cluster","aws_subnet.subnet-private1","aws_subnet.subnet-private2"]
  internal        = false
  security_groups = ["${aws_security_group.load_balancer.id}"]
  subnets         = ["${aws_subnet.subnet-public1.id}", "${aws_subnet.subnet-public2.id}"]

  enable_deletion_protection = false
  tags {
    Name = "${var.environment}-service-alb"
    "ApplicationID" = "pass"
  }
}
//target group
resource "aws_lb_target_group" "auth" {
  depends_on = ["aws_lb_target_group.default"]
  name     = "${var.environment}-${var.authorization_repo_name}"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/api/authorization"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}
resource "aws_lb_target_group" "translation" {
  name     = "${var.environment}-${var.translation_repo_name}"
  depends_on = ["aws_lb_target_group.default"]
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/api/translation"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}
resource "aws_lb_target_group" "game_data" {
  name     = "${var.environment}-${var.game_data_repo_name}"
  depends_on = ["aws_lb_target_group.default"]
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/api/gamedata"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}
resource "aws_lb_target_group" "player_data" {
  name     = "${var.environment}-${var.player_data_repo_name}"
  depends_on = ["aws_lb_target_group.default"]
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/api/playerdata"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}
resource "aws_lb_target_group" "notification" {
  name     = "${var.environment}-${var.notification_repo_name}"
  depends_on = ["aws_lb_target_group.default"]
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/api/notification"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}
resource "aws_lb_target_group" "fleet_management" {
  name     = "${var.environment}-${var.fleet_management_repo_name}"
  depends_on = ["aws_lb_target_group.default"]
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/api/fleet-management"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}
resource "aws_lb_target_group" "platform_management" {
  name     = "${var.environment}-${var.platform_management_repo_name}"
  depends_on = ["aws_lb_target_group.default"]
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/api/platform-management"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}
resource "aws_lb_target_group" "matchmaking" {
  name     = "${var.environment}-${var.matchmaking_repo_name}"
  depends_on = ["aws_lb_target_group.default"]
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/api/matchmaking"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}
resource "aws_lb_target_group" "default" {
  name     = "${var.environment}-default"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.alpha-environment.id}"
  deregistration_delay = 10
  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 10
    path = "/"
    interval = 30
  }
  tags {
    "ApplicationID" = "pass"
  }
}

//Listener
resource "aws_lb_listener" "http_listener" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  load_balancer_arn = "${aws_lb.services-alb.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.default.arn}"
    type             = "forward"
  }
}
resource "aws_lb_listener_rule" "auth" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.auth.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/api/authorization*"]
  }
}
resource "aws_lb_listener_rule" "token" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.auth.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/connect/token*"]
  }
}

resource "aws_lb_listener_rule" "game_data" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.game_data.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/api/gamedata*"]
  }
}
resource "aws_lb_listener_rule" "translation" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.translation.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/api/translation*"]
  }
}
resource "aws_lb_listener_rule" "notification" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.notification.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/api/notification*"]
  }
}
resource "aws_lb_listener_rule" "fleet_management" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.fleet_management.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/api/fleet-management*"]
  }
}
resource "aws_lb_listener_rule" "matchmaking" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.matchmaking.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/api/matchmaking*"]
  }
}
resource "aws_lb_listener_rule" "platform_management" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.platform_management.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/api/platform-management*"]
  }
}
resource "aws_lb_listener_rule" "player_data" {
  depends_on = ["aws_lb_target_group.default", "aws_lb.services-alb"]
  listener_arn  = "${aws_lb_listener.http_listener.arn}"

action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.player_data.arn}"
  }

condition {
    field = "path-pattern"
    values = ["*/api/playerdata*"]
  }
}
