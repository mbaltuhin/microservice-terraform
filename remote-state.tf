terraform {
  backend "s3" {
    bucket = "terraform-devel-remote-state"
    key = "state.file.tfstate"
    dynamodb_table = "terraform-state-lock-dynamo"
    region = "eu-west-1"
  }
}
