resource "aws_elasticache_subnet_group" "subnetgroup-private" {
    name        = "${var.environment}-redis"
    description = "${var.environment}-redis"
    depends_on = ["aws_subnet.subnet-private1", "aws_subnet.subnet-private2"]
    subnet_ids  = ["${aws_subnet.subnet-private1.id}", "${aws_subnet.subnet-private2.id}"]
}
resource "aws_elasticache_cluster" "alpha-redis" {
    depends_on = ["aws_elasticache_subnet_group.subnetgroup-private", "aws_security_group.redis"]
    cluster_id           = "${var.environment}-authorization"
    engine               = "redis"
    engine_version       = "3.2.10"
    node_type            = "cache.t2.small"
    num_cache_nodes      = 1
    parameter_group_name = "default.redis3.2"
    port                 = 6379
    subnet_group_name    = "${aws_elasticache_subnet_group.subnetgroup-private.name}"
    security_group_ids   = ["${aws_security_group.redis.id}"]
    tags {
      "ApplicationID" = "pass"
    }
}
# resource "aws_elasticache_cluster" "alpha-redis2" {
#     depends_on = ["aws_elasticache_subnet_group.subnetgroup-private", "aws_security_group.redis"]
#     cluster_id           = "${var.environment}-translation"
#     engine               = "redis"
#     engine_version       = "3.2.10"
#     node_type            = "cache.t2.small"
#     num_cache_nodes      = 1
#     parameter_group_name = "default.redis3.2"
#     port                 = 6379
#     subnet_group_name    = "${aws_elasticache_subnet_group.subnetgroup-private.name}"
#     security_group_ids   = ["${aws_security_group.redis.id}"]
#     tags {
#       "ApplicationID" = "pass"
#     }
# }
# resource "aws_elasticache_cluster" "alpha-redis3" {
#     depends_on = ["aws_elasticache_subnet_group.subnetgroup-private", "aws_security_group.redis"]
#     cluster_id           = "${var.environment}-platform"
#     engine               = "redis"
#     engine_version       = "3.2.10"
#     node_type            = "cache.t2.small"
#     num_cache_nodes      = 1
#     parameter_group_name = "default.redis3.2"
#     port                 = 6379
#     subnet_group_name    = "${aws_elasticache_subnet_group.subnetgroup-private.name}"
#     security_group_ids   = ["${aws_security_group.redis.id}"]
#     tags {
#       "ApplicationID" = "pass"
#     }
# }
