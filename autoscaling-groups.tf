resource "aws_autoscaling_group" "alpha-game-cluster" {
    desired_capacity          = 0
    depends_on = ["aws_launch_configuration.alpha-game-hosts"]
    health_check_grace_period = 60
    health_check_type         = "EC2"
    launch_configuration      = "${aws_launch_configuration.alpha-game-hosts.name}"
    max_size                  = 0
    min_size                  = 0
    name                      = "${var.environment}-game-cluster"
    vpc_zone_identifier       = ["${aws_subnet.subnet-public1.id}", "${aws_subnet.subnet-public2.id}"]

    tag {
        key   = "Name"
        value = "${var.environment}-game-host"
        propagate_at_launch = true
    }
    tag {
        key   = "ApplicationID"
        value = "pass"
        propagate_at_launch = true
    }
    tag {
        key   = "Schedule"
        value = ""
        propagate_at_launch = true
    }

}

resource "aws_autoscaling_group" "alpha-service-cluster" {
    depends_on = ["aws_launch_configuration.alpha-service-hosts"]
    desired_capacity          = 1
    health_check_grace_period = 60
    health_check_type         = "EC2"
    launch_configuration      = "${aws_launch_configuration.alpha-service-hosts.name}"
    max_size                  = 1
    min_size                  = 1
    name                      = "${var.environment}-service-cluster"
    vpc_zone_identifier       = ["${aws_subnet.subnet-private1.id}", "${aws_subnet.subnet-private2.id}"]

    tag {
        key   = "Name"
        value = "${var.environment}-service-host"
        propagate_at_launch = true
    }
    tag {
        key   = "ApplicationID"
        value = "pass"
        propagate_at_launch = true
    }
    tag {
        key   = "Schedule"
        value = ""
        propagate_at_launch = true
    }

}
resource "aws_autoscaling_group" "alpha-admin-tool" {
    depends_on = ["aws_autoscaling_group.alpha-service-cluster"]
    desired_capacity          = 0
    health_check_grace_period = 60
    health_check_type         = "EC2"
    launch_configuration      = "${aws_launch_configuration.alpha-admin-tool.name}"
    max_size                  = 1
    min_size                  = 0
    name                      = "${var.environment}-admin-tool"
    vpc_zone_identifier       = ["${aws_subnet.subnet-public1.id}", "${aws_subnet.subnet-public2.id}"]

    tag {
        key   = "Name"
        value = "${var.environment}-admin-tool-host"
        propagate_at_launch = true
    }
    tag {
        key   = "ApplicationID"
        value = "pass"
        propagate_at_launch = true
    }
    tag {
        key   = "Schedule"
        value = ""
        propagate_at_launch = true
    }

}
