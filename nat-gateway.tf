resource "aws_nat_gateway" "nat" {
  depends_on = ["aws_internet_gateway.alpha-gateway","aws_eip.nat_eip"]
  allocation_id = "${aws_eip.nat_eip.id}"
  subnet_id     = "${aws_subnet.subnet-public1.id}"

  tags {
    Name = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_eip" "nat_eip" {
  vpc      = true
}
