resource "aws_subnet" "subnet-private1" {
    depends_on = ["aws_vpc.alpha-environment"]
    vpc_id                  = "${aws_vpc.alpha-environment.id}"
    cidr_block              = "173.0.1.0/24"
    availability_zone = "${data.aws_availability_zones.available.names[0]}"
    map_public_ip_on_launch = true

    tags {
        "Name" = "${var.environment}-service-private-1"
        "ApplicationID" = "pass"
    }
}

resource "aws_subnet" "subnet-private2" {
    depends_on = ["aws_vpc.alpha-environment"]
    vpc_id                  = "${aws_vpc.alpha-environment.id}"
    cidr_block              = "173.0.2.0/24"
    availability_zone = "${data.aws_availability_zones.available.names[1]}"
    map_public_ip_on_launch = true

    tags {
        "Name" = "alpha-service-private-2"
        "ApplicationID" = "pass"
    }
}
resource "aws_subnet" "subnet-public1" {
    depends_on = ["aws_vpc.alpha-environment"]
    vpc_id                  = "${aws_vpc.alpha-environment.id}"
    cidr_block              = "173.0.3.0/24"
    availability_zone = "${data.aws_availability_zones.available.names[0]}"
    map_public_ip_on_launch = true

    tags {
        "Name" = "${var.environment}-game-public-1"
        "ApplicationID" = "pass"
    }
}
resource "aws_subnet" "subnet-public2" {
    depends_on = ["aws_vpc.alpha-environment"]
    vpc_id                  = "${aws_vpc.alpha-environment.id}"
    cidr_block              = "173.0.4.0/24"
    availability_zone = "${data.aws_availability_zones.available.names[1]}"
    map_public_ip_on_launch = true

    tags {
        "Name" = "${var.environment}-game-public-2"
        "ApplicationID" = "pass"
    }
}
