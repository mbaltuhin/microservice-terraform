resource "aws_elasticsearch_domain" "es" {
  domain_name           = "${var.environment}"
  elasticsearch_version = "6.2"
  cluster_config {
    instance_type = "t2.small.elasticsearch"
  }
  ebs_options {
    ebs_enabled = true
    volume_size = 35
  }
  vpc_options {
    subnet_ids = ["${aws_subnet.subnet-public1.id}"]
    security_group_ids = ["${aws_security_group.es.id}"]
  }

  advanced_options {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  access_policies = <<CONFIG
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "AWS": "*"
        },
        "Action": "es:*",
        "Resource": "arn:aws:es:${var.region}:285390311874:domain/${var.environment}/*"
      }
    ]
  }
CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags {
    Domain = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
