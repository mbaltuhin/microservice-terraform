resource "aws_internet_gateway" "alpha-gateway" {
  depends_on = ["aws_vpc.alpha-environment"]
  vpc_id = "${aws_vpc.alpha-environment.id}"

    tags {
        "Name" = "${var.environment}-gateway"
        "ApplicationID" = "pass"
    }
}
