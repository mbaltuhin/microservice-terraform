resource "aws_efs_file_system" "efs" {
  performance_mode = "generalPurpose"
  tags {
    Name = "${var.environment}"
  }
}
resource "aws_efs_mount_target" "private1" {
  file_system_id = "${aws_efs_file_system.efs.id}"
  subnet_id      = "${aws_subnet.subnet-private1.id}"
  security_groups = ["${aws_security_group.efs_volume.id}"]
}
resource "aws_efs_mount_target" "private2" {
  file_system_id = "${aws_efs_file_system.efs.id}"
  subnet_id      = "${aws_subnet.subnet-private2.id}"
  security_groups = ["${aws_security_group.efs_volume.id}"]
}
