resource "aws_iam_role" "codebuild_role" {
  name = "${var.environment}-codebuild-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "codebuild.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "codebuild_policy" {
  name        = "${var.environment}-codebuild-policy"
  path        = "/service-role/"
  description = "Policy used in trust relationship with CodeBuild"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Resource": "*",
      "Action": "*"
    }
  ]
}
POLICY
}


resource "aws_iam_policy_attachment" "codebuild_policy_attachment" {
  name       = "${var.environment}-codebuild-policy-attachment"
  policy_arn = "${aws_iam_policy.codebuild_policy.arn}"
  roles      = ["${aws_iam_role.codebuild_role.id}"]
}
resource "aws_codebuild_project" "authorization" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_db_instance.mssql","aws_elasticache_cluster.alpha-redis"]
  name         = "${var.environment}-${var.authorization_repo_name}"
  description  = "BuildProject for ${var.environment}-${var.authorization_repo_name} service"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:17.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      "name"  = "REGION"
      "value" = "${data.aws_region.current.name}"
    }
    environment_variable {
      "name"  = "REPO"
      "value" = "${aws_ecr_repository.authorization.repository_url}"
    }
    environment_variable {
      "name"  = "CLUSTER"
      "value" = "${aws_ecs_cluster.services.name}"
    }
    environment_variable {
      "name"  = "SERVICE"
      "value" = "${aws_ecs_service.auth.name}"
    }
    environment_variable {
      "name"  = "TASK_DEF"
      "value" = "${aws_ecs_service.auth.name}-taskdef"
    }
    environment_variable {
      "name"  = "CONTAINER_NAME"
      "value" = "${aws_ecs_service.auth.name}-container"
    }
    environment_variable {
      "name"  = "VERSION"
      "value" = "0.0.1"
    }
    environment_variable {
      "name"  = "CONNECTION_STRING"
      "value" = "server=${aws_db_instance.mssql.address};user=root;password=rootpassword;Initial Catalog=${aws_ecs_service.auth.name};MultipleActiveResultSets=true;Max Pool Size=500;Pooling=true;connect timeout=60"
    }
    environment_variable {
      "name"  = "ISSUER_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "ISSUER_URL_PROTOCOL"
      "value" = "http://"
    }
    environment_variable {
      "name"  = "ENABLE_MIGRATION"
      "value" = "true"
    }
    environment_variable {
      "name"  = "REDIS_ENDPOINT"
      "value" = "${aws_elasticache_cluster.alpha-redis.cache_nodes.0.address}:${aws_elasticache_cluster.alpha-redis.cache_nodes.0.port}"
    }
    environment_variable {
      "name"  = "ENABLE_SWAGGER"
      "value" = "true"
    }

  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2

phases:
  pre_build:
    commands:
      - $(aws ecr get-login --no-include-email)
  build:
    commands: >
      case $CODEBUILD_BUILD_ARN in
      *build/${var.environment}-authorization*)
      export COMMIT="$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)" && export TAG="$VERSION-$COMMIT-$(echo $(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c10))" &&
      docker build --rm -t $REPO:$TAG . &&
      docker push $REPO:$TAG &&
      aws ecs register-task-definition
      --volumes "[{\"name\":\"LogFolder\",\"host\":{\"sourcePath\":\"/home/ec2-user/Logs\"}}]" --family $TASK_DEF --container-definitions
      "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$REPO:$TAG\",\"command\":[\"python3\",\"/app/config.py\",\"--connection-string=$CONNECTION_STRING\",\"--issuer-url=$ISSUER_URL\",\"--issuer-url-protocol="$ISSUER_URL_PROTOCOL"\",\"--enable-migration=$ENABLE_MIGRATION\",\"--redis-endpoint=$REDIS_ENDPOINT\",\"--enable-swagger=$ENABLE_SWAGGER\"],\"memory\":256,
      \"portMappings\":[{\"containerPort\":80,\"hostPort\":0,\"protocol\":\"tcp\"}],\"essential\":true,\"dnsServers\": [\"127.0.0.1\",\"173.0.0.2\"],
      \"mountPoints\":[{\"sourceVolume\":\"LogFolder\",\"containerPath\":\"/LogFolder\",\"readOnly\":false}]}]" &&
      aws ecs update-service --service $SERVICE --cluster $CLUSTER --task-definition $TASK_DEF ;;
      build/alpha-translation*) echo "Building for alpha environment" ;;
      *) echo "No idea" ;;
      esac
EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_codebuild_project" "matchmaking" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_db_instance.mssql","aws_elasticache_cluster.alpha-redis"]
  name         = "${var.environment}-${var.matchmaking_repo_name}"
  description  = "BuildProject for ${var.matchmaking_repo_name} service"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:17.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      "name"  = "REGION"
      "value" = "${data.aws_region.current.name}"
    }
    environment_variable {
      "name"  = "REPO"
      "value" = "${aws_ecr_repository.matchmaking.repository_url}"
    }
    environment_variable {
      "name"  = "CLUSTER"
      "value" = "${aws_ecs_cluster.services.name}"
    }
    environment_variable {
      "name"  = "SERVICE"
      "value" = "${aws_ecs_service.matchmaking.name}"
    }
    environment_variable {
      "name"  = "TASK_DEF"
      "value" = "${aws_ecs_service.matchmaking.name}-taskdef"
    }
    environment_variable {
      "name"  = "CONTAINER_NAME"
      "value" = "${aws_ecs_service.matchmaking.name}-container"
    }
    environment_variable {
      "name"  = "VERSION"
      "value" = "0.0.1"
    }
    environment_variable {
      "name"  = "CONNECTION_STRING"
      "value" = "server=${aws_db_instance.mssql.address};user=root;password=rootpassword;Initial Catalog=${aws_ecs_service.matchmaking.name};MultipleActiveResultSets=true;Max Pool Size=500;Pooling=true;connect timeout=60"
    }
    environment_variable {
      "name"  = "ISSUER_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "ISSUER_URL_PROTOCOL"
      "value" = "http://"
    }
    environment_variable {
      "name"  = "ENABLE_SWAGGER"
      "value" = "true"
    }
    environment_variable {
      "name"  = "ENABLE_MIGRATION"
      "value" = "true"
    }
  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2

phases:
  pre_build:
    commands:
      - $(aws ecr get-login --no-include-email)
  build:
    commands: >
      case $CODEBUILD_BUILD_ARN in
      *build/${var.environment}-matchmaking*)
      export COMMIT="$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)" && export TAG="$VERSION-$COMMIT-$(echo $(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c10))" &&
      docker build --rm -t $REPO:$TAG . &&
      docker push $REPO:$TAG &&
      aws ecs register-task-definition
      --volumes "[{\"name\":\"LogFolder\",\"host\":{\"sourcePath\":\"/home/ec2-user/Logs\"}}]" --family $TASK_DEF --container-definitions
      "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$REPO:$TAG\",\"command\":[\"python3\",\"/app/config.py\",\"--issuer-url=$VALID_ISSUER\",\"--issuer-url-protocol=$ISSUER_URL_PROTOCOL\",\"--connection-string=$DB_CONNECTION_STRING\",\"--enable-swagger=$ENABLE_SWAGGER\"],\"memory\":256,
      \"portMappings\":[{\"containerPort\":80,\"hostPort\":0,\"protocol\":\"tcp\"}],\"essential\":true,\"dnsServers\": [\"127.0.0.1\",\"173.0.0.2\"],
      \"mountPoints\":[{\"sourceVolume\":\"LogFolder\",\"containerPath\":\"/LogFolder\",\"readOnly\":false}]}]" &&
      aws ecs update-service --service $SERVICE --cluster $CLUSTER --task-definition $TASK_DEF ;;
      build/alpha-translation*) echo "Building for alpha environment" ;;
      *) echo "No idea" ;;
      esac
EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_codebuild_project" "platform_management" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_db_instance.mssql","aws_elasticache_cluster.alpha-redis"]
  name         = "${var.environment}-${var.platform_management_repo_name}"
  description  = "BuildProject for ${var.platform_management_repo_name} service"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:17.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      "name"  = "REGION"
      "value" = "${data.aws_region.current.name}"
    }

    environment_variable {
      "name"  = "REPO"
      "value" = "${aws_ecr_repository.platform_management.repository_url}"
    }
    environment_variable {
      "name"  = "CLUSTER"
      "value" = "${aws_ecs_cluster.services.name}"
    }
    environment_variable {
      "name"  = "SERVICE"
      "value" = "${aws_ecs_service.platform_management.name}"
    }
    environment_variable {
      "name"  = "TASK_DEF"
      "value" = "${aws_ecs_service.platform_management.name}-taskdef"
    }
    environment_variable {
      "name"  = "CONTAINER_NAME"
      "value" = "${aws_ecs_service.platform_management.name}-container"
    }
    environment_variable {
      "name"  = "VERSION"
      "value" = "0.0.1"
    }
    environment_variable {
      "name"  = "ENV"
      "value" = "${var.environment}"
    }
    environment_variable {
      "name"  = "CONFIGURATION"
      "value" = "Configuration-alpha"
    }
    environment_variable {
      "name"  = "REALM"
      "value" = "Realm-alpha"
    }
    environment_variable {
      "name"  = "EC_ENDPOINT"
      "value" = "${aws_elasticache_cluster.alpha-redis.cache_nodes.0.address}:${aws_elasticache_cluster.alpha-redis.cache_nodes.0.port}"
    }
    environment_variable {
      "name"  = "CONNECTION_STRING"
      "value" = "server=${aws_db_instance.mssql.address};user=root;password=rootpassword;Initial Catalog=${aws_ecs_service.auth.name};MultipleActiveResultSets=true;Max Pool Size=500;Pooling=true;connect timeout=60"
    }
    environment_variable {
      "name"  = "ISSUER_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "ISSUER_URL_PROTOCOL"
      "value" = "http://"
    }
    environment_variable {
      "name"  = "ENABLE_SWAGGER"
      "value" = "true"
    }
    environment_variable {
      "name"  = "ENABLE_MIGRATION"
      "value" = "true"
    }
  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2

phases:
  pre_build:
    commands:
      - $(aws ecr get-login --no-include-email)
  build:
    commands: >
      case $CODEBUILD_BUILD_ARN in
      *build/${var.environment}-platform-management*)
      export COMMIT="$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)" && export TAG="$VERSION-$COMMIT-$(echo $(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c10))" &&
      docker build --rm -t $REPO:$TAG . &&
      docker push $REPO:$TAG &&
      aws ecs register-task-definition
      --volumes "[{\"name\":\"LogFolder\",\"host\":{\"sourcePath\":\"/home/ec2-user/Logs\"}}]" --family $TASK_DEF --container-definitions
      "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$REPO:$TAG\",\"command\":[\"python3\",\"/app/config.py\",\"--issuer-url=$ISSUER_URL\",\"--connection-string=$CONNECTION_STRING\",\"--issuer-url-protocol=$ISSUER_URL_PROTOCOL\",\"--redis-endpoint=$EC_ENDPOINT\",\"--enable-migration=$ENABLE_MIGRATION\",\"--enable-swagger=$ENABLE_SWAGGER\"],\"memory\":256,
      \"portMappings\":[{\"containerPort\":80,\"hostPort\":0,\"protocol\":\"tcp\"}],\"essential\":true,\"dnsServers\": [\"127.0.0.1\",\"173.0.0.2\"],
      \"mountPoints\":[{\"sourceVolume\":\"LogFolder\",\"containerPath\":\"/LogFolder\",\"readOnly\":false}]}]" &&
      aws ecs update-service --service $SERVICE --cluster $CLUSTER --task-definition $TASK_DEF ;;
      build/alpha-translation*) echo "Building for alpha environment" ;;
      *) echo "No idea" ;;
      esac
EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_codebuild_project" "fleet_management" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_db_instance.mssql","aws_elasticache_cluster.alpha-redis","aws_efs_file_system.efs"]
  name         = "${var.environment}-${var.fleet_management_repo_name}"
  description  = "BuildProject for ${var.fleet_management_repo_name} service"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:17.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      "name"  = "REGION"
      "value" = "${data.aws_region.current.name}"
    }
    environment_variable {
      "name"  = "REPO"
      "value" = "${aws_ecr_repository.fleet_management.repository_url}"
    }
    environment_variable {
      "name"  = "CLUSTER"
      "value" = "${aws_ecs_cluster.services.name}"
    }
    environment_variable {
      "name"  = "SERVICE"
      "value" = "${aws_ecs_service.fleet_management.name}"
    }
    environment_variable {
      "name"  = "TASK_DEF"
      "value" = "${aws_ecs_service.fleet_management.name}-taskdef"
    }
    environment_variable {
      "name"  = "CONTAINER_NAME"
      "value" = "${aws_ecs_service.fleet_management.name}-container"
    }
    environment_variable {
      "name"  = "VERSION"
      "value" = "0.0.1"
    }
    environment_variable {
      "name"  = "CONNECTION_STRING"
      "value" = "server=${aws_db_instance.mssql.address};user=root;password=rootpassword;Initial Catalog=${aws_ecs_service.fleet_management.name};MultipleActiveResultSets=true;Max Pool Size=500;Pooling=true;connect timeout=60"
    }
    environment_variable {
      "name"  = "ISSUER_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "ISSUER_URL_PROTOCOL"
      "value" = "http://"
    }
    environment_variable {
      "name"  = "ENABLE_MIGRATION"
      "value" = "true"
    }
    environment_variable {
      "name"  = "ENABLE_SWAGGER"
      "value" = "true"
    }
    environment_variable {
      "name"  = "INSTANCE"
      "value" = "m5.large"
    }
    environment_variable {
      "name"  = "SG"
      "value" = "${aws_security_group.game-servers.id}"
    }
    environment_variable {
      "name"  = "VPCZONEID"
      "value" = "${aws_subnet.subnet-public1.id},${aws_subnet.subnet-public2.id}"
    }
    environment_variable {
      "name"  = "MAXSIZE"
      "value" = "1"
    }
    environment_variable {
      "name"  = "MINSIZE"
      "value" = "1"
    }
    environment_variable {
      "name"  = "SERVICEDC"
      "value" = "1"
    }
    environment_variable {
      "name"  = "VPCID"
      "value" = "${aws_vpc.alpha-environment.id}"
    }
    environment_variable {
      "name"  = "ELBSG"
      "value" = "${aws_security_group.game_load_balancer.id}"
    }
    environment_variable {
      "name"  = "ELBSUBNETS"
      "value" = "${aws_subnet.subnet-public1.id},${aws_subnet.subnet-public2.id}"
    }
    environment_variable {
      "name"  = "ROLEARN"
      "value" = "${aws_iam_role.core_instance_role.arn}"
    }
    environment_variable {
      "name"  = "INSTANCEPROFILEARN"
      "value" = "${aws_iam_instance_profile.core_instance.arn}"
    }
    environment_variable {
      "name"  = "DATACENTERID"
      "value" = "foo"
    }
    environment_variable {
      "name"  = "KEY_PAIR"
      "value" = "${var.key_pair_name}"
    }
    environment_variable {
      "name"  = "AMI_ID"
      "value" = "${data.aws_ami.amazon-linux.id}"
    }
    environment_variable {
      "name"  = "ECR_REPO_NAME"
      "value" = "${aws_ecr_repository.game.name}"
    }
    environment_variable {
      "name"  = "ENV"
      "value" = "${var.environment}"
    }
    environment_variable {
      "name"  = "EFS_URL"
      "value" = "${aws_efs_file_system.efs.dns_name}"
    }
    environment_variable {
      "name"  = "CONFIGURATION_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "CORE_SOFT_LIMIT"
      "value" = "-1"
    }
    environment_variable {
      "name"  = "CORE_HARD_LIMIT"
      "value" = "-1"
    }
    environment_variable {
      "name"  = "NOFILE_SOFT_LIMIT"
      "value" = "16384"
    }
    environment_variable {
      "name"  = "NOFILE_HARD_LIMIT"
      "value" = "16384"
    }
    environment_variable {
      "name"  = "EC2NAME"
      "value" = "alpha-game-server"
    }
    environment_variable {
      "name"  = "MEMORY_RESERVATION_SOFT"
      "value" = "512"
    }
    environment_variable {
      "name"  = "MEMORY_RESERVATION_HARD"
      "value" = "512"
    }
    environment_variable {
      "name"  = "MEMORY"
      "value" = "512"
    }
  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2

phases:
  pre_build:
    commands:
      - $(aws ecr get-login --no-include-email)
  build:
    commands: >
      case $CODEBUILD_BUILD_ARN in
      *build/${var.environment}-fleet-management*)
      export COMMIT="$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)" && export TAG="$VERSION-$COMMIT-$(echo $(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c10))" &&
      docker build . -t $REPO:$TAG &&
      docker push $REPO:$TAG &&
      aws ecs register-task-definition
      --volumes "[{\"name\":\"LogFolder\",\"host\":{\"sourcePath\":\"/home/ec2-user/Logs\"}}, {\"name\":\"DataFolder\",\"host\":{\"sourcePath\":\"/mnt\"}}]"
      --family $TASK_DEF
      --container-definitions "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$REPO:$TAG\",\"command\":[
                \"python3\",
                \"/app/config.py\",
                \"--launch-memory=$MEMORY\",
                \"--launch-instance-type=$INSTANCE\",
                \"--launch-security-groups=$SG\",
                \"--launch-az-list=$VPCZONEID\",
                \"--launch-max-group-size=$MAXSIZE\",
                \"--launch-min-group-size=$MINSIZE\",
                \"--launch-service-desired-count=$SERVICEDC\",
                \"--launch-vpc-id=$VPCID\",
                \"--launch-elb-security-groups=$ELBSG\",
                \"--launch-subnets=$ELBSUBNETS\",
                \"--launch-role-arn=$ROLEARN\",
                \"--launch-instance-profile-arn=$INSTANCEPROVILEARN\",
                \"--launch-data-center-id=$DATACENTERID\",
                \"--environment=$ENV\",
                \"--connection-string=$CONNECTION_STRING\",
                \"--issuer-url=$ISSUER_URL\",
                \"--issuer-url-protocol=$ISSUER_URL_PROTOCOL\",
                \"--launch-ami-id=$AMI_ID\",
                \"--launch-key-pair=$KEY_PAIR\",
                \"--ecr-repo-name=$ECR_REPO_NAME\",
                \"--region=$REGION\",
                \"--launch-efs-url=$EFS_URL\",
                \"--configuration-url=$CONFIGURATION_URL\",
                \"--ulimits-core-soft-limit=$CORE_SOFT_LIMIT\",
                \"--ulimits-core-hard-limit=$CORE_HARD_LIMIT\",
                \"--ulimits-nofile-soft-limit=$NOFILE_SOFT_LIMIT\",
                \"--ulimits-nofile-hard-limit=$NOFILE_HARD_LIMIT\",
                \"--launch-ec2-name-tag=$EC2NAME\",
                \"--enable-migration=$ENABLE_MIGRATION\",
                \"--enable-swagger=$ENABLE_SWAGGER\"
            ],\"memory\":256,
      \"portMappings\":[{\"containerPort\":80,\"hostPort\":0,\"protocol\":\"tcp\"}],\"essential\":true,\"dnsServers\": [\"127.0.0.1\",\"173.0.0.2\"],
      \"mountPoints\":[{\"sourceVolume\":\"LogFolder\",\"containerPath\":\"/LogFolder\",\"readOnly\":false},{\"sourceVolume\":\"DataFolder\",\"containerPath\":\"/DataFolder\",\"readOnly\":false}]}]" &&
      aws ecs update-service --service $SERVICE --cluster $CLUSTER --task-definition $TASK_DEF ;;
      build/alpha-translation*) echo "Building for alpha environment" ;;
      *) echo "No idea" ;;
      esac

EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_codebuild_project" "notification" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_elasticache_cluster.alpha-redis","aws_elasticache_cluster.alpha-redis"]
  name         = "${var.environment}-${var.notification_repo_name}"
  description  = "BuildProject for ${var.notification_repo_name} service"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:17.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      "name"  = "REGION"
      "value" = "${data.aws_region.current.name}"
    }

    environment_variable {
      "name"  = "REPO"
      "value" = "${aws_ecr_repository.notification.repository_url}"
    }
    environment_variable {
      "name"  = "CLUSTER"
      "value" = "${aws_ecs_cluster.services.name}"
    }
    environment_variable {
      "name"  = "SERVICE"
      "value" = "${aws_ecs_service.notification.name}"
    }
    environment_variable {
      "name"  = "TASK_DEF"
      "value" = "${aws_ecs_service.notification.name}-taskdef"
    }
    environment_variable {
      "name"  = "CONTAINER_NAME"
      "value" = "${aws_ecs_service.notification.name}-container"
    }
    environment_variable {
      "name"  = "VERSION"
      "value" = "0.0.1"
    }
    environment_variable {
      "name"  = "ISSUER_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "ISSUER_URL_PROTOCOL"
      "value" = "http://"
    }
    environment_variable {
      "name"  = "QUEUE"
      "value" = "${aws_sqs_queue.sqs.name}"
    }
    environment_variable {
      "name"  = "ENABLE_SWAGGER"
      "value" = "true"
    }

  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2

phases:
  pre_build:
    commands:
      - $(aws ecr get-login --no-include-email)
  build:
    commands: >
      case $CODEBUILD_BUILD_ARN in
      *build/${var.environment}-notification*)
      export COMMIT="$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)" && export TAG="$VERSION-$COMMIT-$(echo $(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c10))" &&
      docker build --rm -t $REPO:$TAG . &&
      docker push $REPO:$TAG &&
      aws ecs register-task-definition
      --volumes "[{\"name\":\"LogFolder\",\"host\":{\"sourcePath\":\"/home/ec2-user/Logs\"}}]" --family $TASK_DEF --container-definitions
      "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$REPO:$TAG\",\"command\":[\"python3\",\"/app/config.py\",\"--issuer-url=$ISSUER_URL\",\"--issuer-url-protocol=$ISSUER_URL_PROTOCOL\",\"--queue-name=$QUEUE\",\"--enable-swagger=$ENABLE_SWAGGER\"],\"memory\":256,
      \"portMappings\":[{\"containerPort\":80,\"hostPort\":0,\"protocol\":\"tcp\"}],\"essential\":true,\"dnsServers\": [\"127.0.0.1\",\"173.0.0.2\"],
      \"mountPoints\":[{\"sourceVolume\":\"LogFolder\",\"containerPath\":\"/LogFolder\",\"readOnly\":false}]}]" &&
      aws ecs update-service --service $SERVICE --cluster $CLUSTER --task-definition $TASK_DEF ;;
      build/alpha-translation*) echo "Building for alpha environment" ;;
      *) echo "No idea" ;;
      esac
EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_codebuild_project" "translation" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_db_instance.mssql","aws_elasticache_cluster.alpha-redis"]
  name         = "${var.environment}-${var.translation_repo_name}"
  description  = "BuildProject for ${var.translation_repo_name} service"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:17.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      "name"  = "REGION"
      "value" = "${data.aws_region.current.name}"
    }

    environment_variable {
      "name"  = "REPO"
      "value" = "${aws_ecr_repository.translation.repository_url}"
    }
    environment_variable {
      "name"  = "CLUSTER"
      "value" = "${aws_ecs_cluster.services.name}"
    }
    environment_variable {
      "name"  = "SERVICE"
      "value" = "${aws_ecs_service.translation.name}"
    }
    environment_variable {
      "name"  = "TASK_DEF"
      "value" = "${aws_ecs_service.translation.name}-taskdef"
    }
    environment_variable {
      "name"  = "CONTAINER_NAME"
      "value" = "${aws_ecs_service.translation.name}-container"
    }
    environment_variable {
      "name"  = "VERSION"
      "value" = "0.0.1"
    }
    environment_variable {
      "name"  = "EC_ENDPOINT"
      "value" = "${aws_elasticache_cluster.alpha-redis.cache_nodes.0.address}:${aws_elasticache_cluster.alpha-redis.cache_nodes.0.port}"
    }
    environment_variable {
      "name"  = "ISSUER_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "ISSUER_URL_PROTOCOL"
      "value" = "http://"
    }
    environment_variable {
      "name"  = "AZURE_AUTH"
      "value" = "1cd054fb962c451c8e1482e977e0f9dd"
    }
    environment_variable {
      "name"  = "TOKEN_URL"
      "value" = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken"
    }
    environment_variable {
      "name"  = "TRANSLATE_URL"
      "value" = "https://api.microsofttranslator.com/v2/Http.svc/Translate?category=generalnn&text="
    }
    environment_variable {
      "name"  = "SUBS_KEY"
      "value" = "Ocp-Apim-Subscription-Key"
    }
    environment_variable {
      "name"  = "SWAGGER_ENABLED"
      "value" = "true"
    }

  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2

phases:
  pre_build:
    commands:
      - $(aws ecr get-login --no-include-email)
  build:
    commands: >
      case $CODEBUILD_BUILD_ARN in
      *build/${var.environment}-translation*)
      export COMMIT="$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)" && export TAG="$VERSION-$COMMIT-$(echo $(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c10))" &&
      docker build --rm -t $REPO:$TAG . &&
      docker push $REPO:$TAG &&
      aws ecs register-task-definition
      --volumes "[{\"name\":\"LogFolder\",\"host\":{\"sourcePath\":\"/home/ec2-user/Logs\"}}]" --family $TASK_DEF --container-definitions "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$REPO:$TAG\",\"command\":[\"python3\",\"/app/config.py\",\"--issuer-url=$ISSUER_URL\",\"--issuer-url-protocol=$ISSUER_URL_PROTOCOL\",\"--elastic-cache-name=$EC_ENDPOINT\",\"--azure-key=$AZURE_AUTH\",\"--issue-token-url=$TOKEN_URL\",\"--translate-url=$TRANSLATE_URL\",\"--subscription-header=$SUBS_KEY\",\"--enable-swagger=$SWAGGER_ENABLED\"],\"memory\":256,
      \"portMappings\":[{\"containerPort\":80,\"hostPort\":0,\"protocol\":\"tcp\"}],\"essential\":true,\"dnsServers\": [\"127.0.0.1\",\"173.0.0.2\"],
      \"mountPoints\":[{\"sourceVolume\":\"LogFolder\",\"containerPath\":\"/LogFolder\",\"readOnly\":false}]}]" &&
      aws ecs update-service --service $SERVICE --cluster $CLUSTER --task-definition $TASK_DEF ;;
      build/alpha-translation*) echo "Building for alpha environment" ;;
      *) echo "No idea" ;;
      esac
EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_codebuild_project" "game_data" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_elasticache_cluster.alpha-redis","aws_elasticache_cluster.alpha-redis"]
  name         = "${var.environment}-${var.game_data_repo_name}"
  description  = "BuildProject for ${var.game_data_repo_name} service"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:17.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      "name"  = "REGION"
      "value" = "${data.aws_region.current.name}"
    }

    environment_variable {
      "name"  = "REPO"
      "value" = "${aws_ecr_repository.game_data.repository_url}"
    }
    environment_variable {
      "name"  = "CLUSTER"
      "value" = "${aws_ecs_cluster.services.name}"
    }
    environment_variable {
      "name"  = "SERVICE"
      "value" = "${aws_ecs_service.game_data.name}"
    }
    environment_variable {
      "name"  = "TASK_DEF"
      "value" = "${aws_ecs_service.game_data.name}-taskdef"
    }
    environment_variable {
      "name"  = "CONTAINER_NAME"
      "value" = "${aws_ecs_service.game_data.name}-container"
    }
    environment_variable {
      "name"  = "VERSION"
      "value" = "0.0.1"
    }
    environment_variable {
      "name"  = "ISSUER_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "ISSUER_URL_PROTOCOL"
      "value" = "http://"
    }
    environment_variable {
      "name"  = "GSENV"
      "value" = "${var.environment}"
    }
    environment_variable {
      "name"  = "QUEUE"
      "value" = "${aws_sqs_queue.sqs.name}"
    }
    environment_variable {
      "name"  = "CDNACC"
      "value" = "foo"
    }
    environment_variable {
      "name"  = "ENABLE_SWAGGER"
      "value" = "true"
    }

  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2

phases:
  pre_build:
    commands:
      - $(aws ecr get-login --no-include-email)
  build:
    commands: >
      case $CODEBUILD_BUILD_ARN in
      *build/${var.environment}-game-data*)
      export COMMIT="$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)" && export TAG="$VERSION-$COMMIT-$(echo $(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c10))" &&
      docker build --rm -t $REPO:$TAG . &&
      docker push $REPO:$TAG &&
      aws ecs register-task-definition
      --volumes "[{\"name\":\"LogFolder\",\"host\":{\"sourcePath\":\"/home/ec2-user/Logs\"}}]" --family $TASK_DEF --container-definitions
      "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$REPO:$TAG\",\"command\":[\"python3\",\"/app/config.py\",\"--issuer-url=$ISSUER_URL\",\"--issuer-url-protocol=$ISSUER_URL_PROTOCOL\",\"--game-server-environment=$GSENV\",\"--queue-name=$QUEUE\",\"--account-id=$CDNACC\",\"--enable-swagger=$ENABLE_SWAGGER\"],\"memory\":256,
      \"portMappings\":[{\"containerPort\":80,\"hostPort\":0,\"protocol\":\"tcp\"}],\"essential\":true,\"dnsServers\": [\"127.0.0.1\",\"173.0.0.2\"],
      \"mountPoints\":[{\"sourceVolume\":\"LogFolder\",\"containerPath\":\"/LogFolder\",\"readOnly\":false}]}]" &&
      aws ecs update-service --service $SERVICE --cluster $CLUSTER --task-definition $TASK_DEF ;;
      build/alpha-translation*) echo "Building for alpha environment" ;;
      *) echo "No idea" ;;
      esac
EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_codebuild_project" "player_data" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_elasticache_cluster.alpha-redis","aws_elasticache_cluster.alpha-redis"]
  name         = "${var.environment}-${var.player_data_repo_name}"
  description  = "BuildProject for ${var.player_data_repo_name} service"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/docker:17.09.0"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

    environment_variable {
      "name"  = "REGION"
      "value" = "${data.aws_region.current.name}"
    }

    environment_variable {
      "name"  = "REPO"
      "value" = "${aws_ecr_repository.player_data.repository_url}"
    }
    environment_variable {
      "name"  = "CLUSTER"
      "value" = "${aws_ecs_cluster.services.name}"
    }
    environment_variable {
      "name"  = "SERVICE"
      "value" = "${aws_ecs_service.player_data.name}"
    }
    environment_variable {
      "name"  = "TASK_DEF"
      "value" = "${aws_ecs_service.player_data.name}-taskdef"
    }
    environment_variable {
      "name"  = "CONTAINER_NAME"
      "value" = "${aws_ecs_service.player_data.name}-container"
    }
    environment_variable {
      "name"  = "VERSION"
      "value" = "0.0.1"
    }
    environment_variable {
      "name"  = "ISSUER_URL"
      "value" = "${var.issuer}"
    }
    environment_variable {
      "name"  = "ISSUER_URL_PROTOCOL"
      "value" = "http://"
    }
    environment_variable {
      "name"  = "QUEUE"
      "value" = "${aws_sqs_queue.sqs.name}"
    }
    environment_variable {
      "name"  = "GSENV"
      "value" = "${var.environment}"
    }
    environment_variable {
      "name"  = "CDNACC"
      "value" = "${var.environment}"
    }
    environment_variable {
      "name"  = "ENABLE_SWAGGER"
      "value" = "true"
    }

  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2

phases:
  pre_build:
    commands:
      - $(aws ecr get-login --no-include-email)
  build:
    commands: >
      case $CODEBUILD_BUILD_ARN in
      *build/${var.environment}-player-data*)
      export COMMIT="$(echo $CODEBUILD_RESOLVED_SOURCE_VERSION | head -c 8)" && export TAG="$VERSION-$COMMIT-$(echo $(< /dev/urandom tr -dc A-Z-a-z-0-9 | head -c10))" &&
      docker build --rm -t $REPO:$TAG . &&
      docker push $REPO:$TAG &&
      aws ecs register-task-definition
      --volumes "[{\"name\":\"LogFolder\",\"host\":{\"sourcePath\":\"/home/ec2-user/Logs\"}}]" --family $TASK_DEF --container-definitions
      "[{\"name\":\"$CONTAINER_NAME\",\"image\":\"$REPO:$TAG\",\"command\":[\"python3\",\"/app/config.py\",\"--issuer-url=$ISSUER_URL\",\"--issuer-url-protocol=$ISSUER_URL_PROTOCOL\",\"--game-server-environment=$GSENV\",\"--queue-name=$QUEUE\",\"--account-id=$CDNACC\",\"--enable-swagger=$ENABLE_SWAGGER\"],\"memory\":256,
      \"portMappings\":[{\"containerPort\":80,\"hostPort\":0,\"protocol\":\"tcp\"}],\"essential\":true,\"dnsServers\": [\"127.0.0.1\",\"173.0.0.2\"],
      \"mountPoints\":[{\"sourceVolume\":\"LogFolder\",\"containerPath\":\"/LogFolder\",\"readOnly\":false}]}]" &&
      aws ecs update-service --service $SERVICE --cluster $CLUSTER --task-definition $TASK_DEF ;;
      build/alpha-translation*) echo "Building for alpha environment" ;;
      *) echo "No idea" ;;
      esac
EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
resource "aws_codebuild_project" "admin" {
  depends_on = ["aws_iam_policy_attachment.codebuild_policy_attachment","aws_elasticache_cluster.alpha-redis","aws_elasticache_cluster.alpha-redis"]
  name         = "admin-tool"
  description  = "BuildProject for admin tool"
  build_timeout      = "5"
  service_role = "${aws_iam_role.codebuild_role.arn}"

  artifacts {
    type = "CODEPIPELINE"
    namespace_type = "NONE"
  }

  environment {
    compute_type = "BUILD_GENERAL1_SMALL"
    image        = "aws/codebuild/nodejs:6.3.1"
    type         = "LINUX_CONTAINER"
    privileged_mode = true

  }

  source {
    type = "CODEPIPELINE"
    buildspec = <<EOF
version: 0.2
phases:
  install:
    commands:
      - npm install grunt grunt-cli bower -g
  build:
    commands:
      - cd ./AdminPanel/ && npm install && bower install --allow-root && grunt build
      - cd ..
      - ls
      - mkdir ./AdminPanel/dist/launch_scripts
      - mv ./appspec.yml ./AdminPanel/dist
      - cp ./scripts/* ./AdminPanel/dist/launch_scripts
artifacts:
  files:
    - '**/*'
  base-directory: ./AdminPanel/dist
EOF
  }

  tags {
    "Environment" = "${var.environment}"
    "ApplicationID" = "pass"
  }
}
