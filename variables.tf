variable "region" {
  default = "eu-west-1"
}
variable "ec2_type" {
  default = "t2.medium"
}
variable "environment" {
  default = "devel"
}
variable "MSSQL_username" {
  default = "root"
}
variable "MSSQL_password" {
  default = "rootpassword"
}
variable "rds_instance_type" {
  default = "db.t2.micro"
}
variable "authorization_repo_name" {
  default = "authorization"
}
variable "matchmaking_repo_name" {
  default = "matchmaking"
}
variable "platform_management_repo_name" {
  default = "platform-management"
}
variable "fleet_management_repo_name" {
  default = "fleet-management"
}
variable "notification_repo_name" {
  default = "notification"
}
variable "admin_repo_name" {
  default = "admin-tools"
}
variable "game_data_repo_name" {
  default = "game-data"
}
variable "game_repo_name" {
  default = "game"
}
variable "translation_repo_name" {
  default = "translation"
}
variable "player_data_repo_name" {
  default = "player-data"
}

variable "key_pair_name" {
  default = "paas-devel"
}
variable "issuer" {
  default = "localhost"
}
variable "repo_bucket" {
  default = "rnd-bitbucket-repos"
}
