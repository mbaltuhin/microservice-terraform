resource "aws_ecr_repository" "authorization" {
  name = "${var.environment}-${var.authorization_repo_name}"
}
resource "aws_ecr_repository" "platform_management" {
  name = "${var.environment}-${var.platform_management_repo_name}"
}
resource "aws_ecr_repository" "fleet_management" {
  name = "${var.environment}-${var.fleet_management_repo_name}"
}
resource "aws_ecr_repository" "matchmaking" {
  name = "${var.environment}-${var.matchmaking_repo_name}"
}
resource "aws_ecr_repository" "notification" {
  name = "${var.environment}-${var.notification_repo_name}"
}
resource "aws_ecr_repository" "translation" {
  name = "${var.environment}-${var.translation_repo_name}"
}
resource "aws_ecr_repository" "game_data" {
  name = "${var.environment}-${var.game_data_repo_name}"
}
resource "aws_ecr_repository" "player_data" {
  name = "${var.environment}-${var.player_data_repo_name}"
}
resource "aws_ecr_repository" "game" {
  name = "${var.environment}-${var.game_repo_name}"
}
