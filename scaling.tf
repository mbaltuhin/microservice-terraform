resource "aws_autoscaling_policy" "services_scale_up" {
  depends_on = ["aws_autoscaling_group.alpha-service-cluster"]
  name                   = "${var.environment}-services-scale-up"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = 1
  autoscaling_group_name = "${aws_autoscaling_group.alpha-service-cluster.name}"
  cooldown               = 300
}


resource "aws_cloudwatch_metric_alarm" "alpha_services_scale_up" {
  depends_on = ["aws_autoscaling_group.alpha-service-cluster"]
  alarm_name          = "${var.environment}-services-cpu-scale-up-1"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "80"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.alpha-service-cluster.name}"
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = ["${aws_autoscaling_policy.services_scale_up.arn}","${aws_appautoscaling_policy.auth_scale_up.arn}","${aws_appautoscaling_policy.platform_management_scale_scale_up.arn}","${aws_appautoscaling_policy.player_data_scale_up.arn}","${aws_appautoscaling_policy.translation_scale_up.arn}"]
}
resource "aws_cloudwatch_metric_alarm" "alpha_services_scale_up_2" {
  depends_on = ["aws_autoscaling_group.alpha-service-cluster"]
  alarm_name          = "${var.environment}-services-cpu-scale-up-2"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "80"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.alpha-service-cluster.name}"
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = ["${aws_appautoscaling_policy.notification_scale_up.arn}","${aws_appautoscaling_policy.matchmaking_scale_up.arn}","${aws_appautoscaling_policy.game_data_scale_up.arn}"]
}

//scale down

resource "aws_autoscaling_policy" "services_scale_down" {
  depends_on = ["aws_autoscaling_group.alpha-service-cluster"]
  name                   = "${var.environment}-services-scale-down"
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = -1
  autoscaling_group_name = "${aws_autoscaling_group.alpha-service-cluster.name}"
  cooldown               = 300
}
resource "aws_cloudwatch_metric_alarm" "alpha_services_scale_down" {
  depends_on = ["aws_autoscaling_group.alpha-service-cluster"]
  alarm_name          = "${var.environment}-services-cpu-scale-down-1"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "30"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.alpha-service-cluster.name}"
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = ["${aws_autoscaling_policy.services_scale_down.arn}","${aws_appautoscaling_policy.auth_scale_down.arn}","${aws_appautoscaling_policy.platform_management_scale_down.arn}","${aws_appautoscaling_policy.player_data_scale_down.arn}","${aws_appautoscaling_policy.translation_scale_down.arn}"]
}
resource "aws_cloudwatch_metric_alarm" "alpha_services_scale_down_2" {
  depends_on = ["aws_autoscaling_group.alpha-service-cluster"]
  alarm_name          = "${var.environment}-services-cpu-scale-down-2"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Average"
  threshold           = "30"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.alpha-service-cluster.name}"
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = ["${aws_appautoscaling_policy.notification_scale_down.arn}","${aws_appautoscaling_policy.matchmaking_scale_down.arn}","${aws_appautoscaling_policy.game_data_scale_down.arn}"]
}
