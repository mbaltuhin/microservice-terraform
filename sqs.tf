resource "aws_sqs_queue" "sqs" {
  name                        = "${var.environment}-${var.notification_repo_name}"
  tags {
    "ApplicationID" = "pass"
  }
}
